# Project 01: Idlebin

This is [Project 01] of [CSE-30341-FA19].

## Student

- Domer McDomerson (dmcdomers@nd.edu)

## Brainstorming

To a look at the provided implementation of `ls` and answer the following questions:

1. How are command line arguments handled?

    > RESPONSE REQUIRED (limit to a **one or two sentences**).

2. Which system calls are used?
    
    > RESPONSE REQUIRED (limit to a **one or two sentences** or a **list**).

3. How are directories traversed?
    
    > RESPONSE REQUIRED (limit to a **one or two sentences**).

Read the [cp] manpage and answer the following questions:

1. What does the `-R` flag mean? the `-v` flag?
    
    > RESPONSE REQUIRED (limit to a **one or two sentences**).

2. How would you handle copying from different types of sources such as a file?
   a directory? a link?
    
    > RESPONSE REQUIRED (limit to a **one or two sentences**).

3. How would you handle copying to different types of targets such as a file? a
   directory?
    
    > RESPONSE REQUIRED (limit to a **few sentences**).

[cp]: http://man7.org/linux/man-pages/man1/cp.1.html

## Applets

### Provided

- [x] `basename NAME [SUFFIX]`

- [x] `cat [-E] [FILES]...`

- [x] `false`

- [x] `ls [-Ra] [PATHS]...`

- [x] `pwd`

- [x] `true`

- [x] `yes [STRING]...`

### Required

- [ ] `cp [-Rv] SOURCE... TARGET`

### Optional

- [ ] `chmod OCTAL-MODE FILE...`

- [ ] `ln [-sv] TARGET LINK_NAME`

- [ ] `mkdir [-v] DIRECTORY...`

- [ ] `rmdir [-v] DIRECTORY...`

- [ ] `touch [FILES]...`

## Reflection

1. Describe the overall design and implementation of your implementation of
   `cp`.  In particular, explain what system calls you utilized and how you
   handle errors or edge cases related to system call failure.
    
    > RESPONSE REQUIRED (limit to a **few sentences**).
    
2. Select one of the system calls you identified in the previous example, and
   describe what happens when `cp` utilizes that system call.  In particular,
   explain the transition between user application mode to operating system
   kernel mode.
    
    > RESPONSE REQUIRED (limit to a **few sentences**).
   
[Project 01]:       https://www3.nd.edu/~pbui/teaching/cse.30341.fa19/project01.html
[CSE-30341-FA19]:   https://www3.nd.edu/~pbui/teaching/cse.30341.fa19/
