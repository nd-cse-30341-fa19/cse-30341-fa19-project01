/* idlebin.h: idlebin header */

#ifndef IDLEBIN_H
#define IDLEBIN_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Macros */

#define debug(M, ...)   \
    fprintf(stderr, "DEBUG %10s:%-4d " M "\n", __FILE__, __LINE__, ##__VA_ARGS__)

#define display_usage(message, status) \
    do { fprintf(stderr, "%s\n", message); exit(status); } while (0);

#define streq(a, b) \
    (strcmp(a, b) == 0)

/* Applet Structure */

typedef int (*AppletFunction)(int argc, char **argv);

typedef struct {
    const char *    name;
    const char *    usage;
    AppletFunction  function;
} Applet;

/* Applet Functions */

#include "idlebin_applets.h"

/* Applet Array */

extern Applet APPLETS[];

#endif

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
