/* basename.c: basename applet */

#include "idlebin.h"

#include <libgen.h>
#include <limits.h>
#include <string.h>

/* Usage */

const char BASENAME_USAGE[] = "Usage: basename NAME [SUFFIX]";

/* Applet */

int basename_applet(int argc, char *argv[]) {
    char *name   = NULL;
    char *suffix = NULL;

    /* Parse command line arguments */
    switch (argc) {
        case 1:
            display_usage(BASENAME_USAGE, EXIT_FAILURE);
            break;
        case 3:
            suffix = argv[2];
            /* Fall-through */
        case 2:
            name   = argv[1];
            break;
        default:
            display_usage(BASENAME_USAGE, EXIT_FAILURE);
    }

    /* Determine basename */
    char path[PATH_MAX];
    strncpy(path, name, PATH_MAX);
    name = basename(path);

    /* Handle suffix */
    if (suffix) {
        size_t nlen = strlen(name);
        size_t slen = strlen(suffix);
        if (slen < nlen && streq(name + nlen - slen, suffix)) {
            name[nlen - slen] = 0;
        }
    }

    /* Display result */
    puts(name);

    return EXIT_SUCCESS;
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
