/* cat.c: cat applet */

#include "idlebin.h"

#include <errno.h>
#include <stdbool.h>
#include <string.h>

/* Usage */

const char CAT_USAGE[] = \
    "Usage: cat [-E] [FILES]...\n" \
    "   -E    Display $ at end of each line";

/* Global Variables */

bool ShowEndings = false;
int  ExitStatus  = 0;

/* Functions */

void cat_stream(FILE *stream) {
    char buffer[BUFSIZ];

    while (fgets(buffer, BUFSIZ, stream)) {
        buffer[strlen(buffer) - 1] = 0;
        printf("%s%s\n", buffer, ShowEndings ? "$" : "");
    }
}

void cat_file(const char *path) {
    FILE *fs = fopen(path, "r");
    if (fs == NULL) {
	fprintf(stderr, "%s: %s\n", path, strerror(errno));
	ExitStatus++;
	return;
    }
    cat_stream(fs);
    fclose(fs);
}

/* Applet */

int cat_applet(int argc, char *argv[]) {
    /* Parse command line arguments */
    int argind = 1;

    while (argind < argc && strlen(argv[argind]) > 1 && argv[argind][0] == '-') {
        char *arg = argv[argind++];
        switch (arg[1]) {
            case 'E':
                ShowEndings = true;
                break;
            case 'h':
                display_usage(CAT_USAGE, EXIT_SUCCESS);
                break;
            default:
                display_usage(CAT_USAGE, EXIT_FAILURE);
                break;
        }
    }

    /* Process each file */
    if (argind == argc) {
        cat_stream(stdin);
    } else {
        while (argind < argc) {
            char *path = argv[argind++];
            if (streq(path, "-")) {
                cat_stream(stdin);
            } else {
                cat_file(path);
            }
        }
    }

    return ExitStatus;
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
