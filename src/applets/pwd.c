/* pwd.c: pwd applet */

#include "idlebin.h"

#include <errno.h>
#include <limits.h>
#include <string.h>

#include <unistd.h>

/* Usage */

const char PWD_USAGE[] = "Usage: pwd";

/* Applet */

int pwd_applet(int argc, char *argv[]) {
    char path[PATH_MAX]; 

    if (!getcwd(path, PATH_MAX)) {
    	fprintf(stderr, "getcwd: %s\n", strerror(errno));
    	return EXIT_FAILURE;
    }

    puts(path);

    return EXIT_SUCCESS;
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
