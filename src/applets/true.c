/* true.c: true applet */

#include "idlebin.h"

/* Usage */

const char TRUE_USAGE[] = "Usage: true"; 

/* Applet */

int true_applet(int argc, char *argv[]) {
    return EXIT_SUCCESS;
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
