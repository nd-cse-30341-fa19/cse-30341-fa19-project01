#!/usr/bin/env python3

import os
import subprocess
import sys
import unittest

from subprocess import PIPE, STDOUT

# Pwd Test Case

class PwdTestCase(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        print('\nTesting pwd...', file=sys.stderr)

    def test_00_help(self):
        command = 'bin/idlebin pwd --help'.split()
        process = subprocess.run(command, stdout=PIPE, stderr=STDOUT)
        self.assertEqual(process.stdout, b'Usage: pwd\n')

    def test_01_curdir(self):
        command = 'bin/idlebin pwd'.split()
        process = subprocess.run(command, stdout=PIPE, stderr=STDOUT)
        self.assertEqual(
            process.stdout.decode('utf-8').strip(),
            os.path.abspath(os.curdir)
        )

    def test_01_curdir_valgrind(self):
        command = 'valgrind --leak-check=full bin/idlebin pwd'.split()
        process = subprocess.run(command, stdout=PIPE, stderr=STDOUT)
        lines   = process.stdout.split(b'\n')
        errors  = [int(l.split()[3]) for l in lines if b'ERROR SUMMARY' in l]

        self.assertEqual(process.returncode, 0)
        self.assertEqual(errors, [0])

# Main Execution

if __name__ == '__main__':
    unittest.main()

# vim: set sts=4 sw=4 ts=8 expandtab ft=python:
