#!/usr/bin/env python3

import os
import subprocess
import sys
import time
import unittest

from subprocess import DEVNULL, PIPE, STDOUT

# Yes Test Case

class YesTestCase(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        print('\nTesting yes...', file=sys.stderr)

    def check_output(self, arguments, expected):
        command = 'bin/idlebin yes {}'.format(arguments)
        process = subprocess.run(command, stdout=PIPE, stderr=STDOUT, shell=True)
        output  = process.stdout.decode('utf-8')

        self.assertEqual(output, expected)
        self.assertEqual(process.returncode, 0)

    def check_valgrind(self, arguments):
        command = 'valgrind --leak-check=full bin/idlebin yes {}'.format(arguments).split()
        process = subprocess.Popen(command, stdout=DEVNULL, stderr=PIPE)
        try:
            _, stderr = process.communicate(timeout=0.2)
        except subprocess.TimeoutExpired:
            process.terminate()
            _, stderr = process.communicate()

        lines   = stderr.split(b'\n')
        errors  = [int(l.split()[3]) for l in lines if b'ERROR SUMMARY' in l]

        self.assertEqual(errors, [0])

    def test_00_help(self):
        command = 'bin/idlebin yes --help'.split()
        process = subprocess.run(command, stdout=PIPE, stderr=STDOUT)
        self.assertEqual(process.stdout, b'Usage: yes [STRING]...\n')

    def test_01_yyy(self):
        self.check_output('| head -n 3', 'y\n'*3)

    def test_01_yyy_valgrind(self):
        self.check_valgrind('')

    def test_02_asdf(self):
        self.check_output('asdf | head -n 3', 'asdf\n'*3)

    def test_02_asdf_valgrind(self):
        self.check_valgrind('asdf')

    def test_03_hello_world(self):
        self.check_output('hello world | head -n 5', 'hello world\n'*5)

    def test_03_hello_world_valgrind(self):
        self.check_valgrind('hello world')

    def test_04_fate_is_inexorable(self):
        self.check_output('fate is inexorable | head -n 100', 'fate is inexorable\n'*100)

    def test_04_fate_is_inexorable_valgrind(self):
        self.check_valgrind('fate is inexorable')

# Main Execution

if __name__ == '__main__':
    unittest.main()

# vim: set sts=4 sw=4 ts=8 expandtab ft=python:
